import { Readable, Writable } from "stream";

import { EHttpStatusCode } from "./HTTPStatusCodes";


// todo: fill'em
export type THTTPRequestHandler = (request: IHTTPRequest) => Promise<IHTTPResponse>;
export type TRequestHeader = 'ContentLength' | 'Cookie' | 'DNT' | 'TE';
export type TResponseHeader = 'ContentLength' | 'Cookie';

export interface IHTTPRequest {
    body: Readable;
    headers: Map<TRequestHeader, any>;
    path: string;
    queryParameters: Map<string, any>;
}

export interface IHTTPResponse {
    body: Writable;
    headers: Map<TResponseHeader, any>;
    status: EHttpStatusCode;
    trailers: Promise<Map<TRequestHeader, any>>;
}

export interface IServer {
    registerHTTPRoute(path: string, handler: THTTPRequestHandler): IServer;
//    registerWSRoute(path: string, handler: TWSRequestHandler): IServer;
    startServer(port: number): IServer;
    stopServer(): void;
}
