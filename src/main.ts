import {createSecureServer, Http2SecureServer} from 'http2';

import {None, Option, Some} from "@usefultools/monads";

import {IHTTPRequest, IServer, THTTPRequestHandler} from "./api";


export class Server implements IServer {
    private httpHandlers: Map<string, THTTPRequestHandler[]>;
    private server: Option<Http2SecureServer>;

    constructor() {
        this.httpHandlers = new Map();
        this.server = None;
    }

    registerHTTPRoute(path: string, handler: THTTPRequestHandler): IServer {
        if (!this.httpHandlers.has(path)) {
            this.httpHandlers.set(path, []);
        }

        const pathHandlers = this.httpHandlers.get(path) || [];

        if (!pathHandlers.includes(handler)) {
            pathHandlers.push(handler);
        }

        // todo: add algorithm, which optimizes path look-ups,
        // - possibly using a tree
        // - parsing from right to left might yield easier logic
        // - put code into new package
        // - hang all handlers as values into the tree
        // - can I do this using wasm?

        return this;
    }

    startServer(port: number = 443): IServer {
        this.server = Some(createSecureServer({}, (req, res) => {
            // first, get path
            // create request object
            const httpRequest: IHTTPRequest = {
                body: req,
                headers: new Map(),
                path: 'TODO',
                queryParameters: new Map(),
            };

            // handle streams
        }).listen(port));

        return this;
    }

    stopServer(): void {
        if (this.server.is_some()) {
            this.server.unwrap().close();
        }
    }

}
